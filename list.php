<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript"
            src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css'
          media="screen"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
          type="text/css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
            type="text/javascript"></script>
</head>
<body>
<?php
$pkhoa = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
?>

<div class="name">
    <label class="lab">Khoa </label>
    <select name="khoa" id="khoa" class="ip khoa search_khoa">
        <option value=""></option>
        <?php
        foreach ($pkhoa as $key => $value) { ?>
            <option value="<?=$value?>"><?=$value?></option>
        <?php };
        ?>
    </select>
</div>
<div class="name">
    <label class="lab">Từ khoá</label>
    <input class="inp" type="text" name="tukhoa"/>
</div>
<button class="dki">Tìm kiếm</button>
<p class="num">Số sinh viên tìm thấy: XXX</p>
<a href="./dangky.php"><input type="button" value="Thêm">
</a>
<table>
    <tr>
        <th>No</th>
        <th>Tên sinh viên</th>
        <th>Khoa</th>
        <th>Action</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Nguyễn Văn A</td>
        <td class="col3">Khoa học máy tính</td>
        <td class="col4">
            <button>Xoá</button>
            <button>Sửa</button>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>Trần Thị B</td>
        <td>Khoa học máy tính</td>
        <td>
            <button>Xoá</button>
            <button>Sửa</button>
        </td>
    </tr>
    <tr>
        <td>3</td>
        <td>Nguyễn Hoàng C</td>
        <td>Khoa học vật liệu</td>
        <td>
            <button>Xoá</button>
            <button>Sửa</button>
        </td>
    </tr>
    <tr>
        <td>4</td>
        <td>Đinh Quang D</td>
        <td>Khoa học vật liệu</td>
        <td>
            <button>Xoá</button>
            <button>Sửa</button>
        </td>
    </tr>
</table>
</body>
</html>
