<?php

session_start();

$data = [];
if (!isset($_SESSION['data'])) {
    header('Location: dangky.php');
} else {
    $data = $_SESSION['data'];
    unset($_SESSION['data']);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css'
          media="screen"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
          type="text/css"/>
    <style>
        .container {
            display: flex;
            height: 100vh;
            justify-content: center;
        }

        .wrapper {
            display: flex;
            justify-content: center;
            flex-direction: column;
        }

        form {
            width: 500px;
            border: 1px solid rgb(188, 208, 246);
            padding: 30px;
        }

        p {
            background: lightgray;
            padding: 8px;
        }

        .name {
            margin: 12px 0px;
            display: flex;
        }

        .gtinh {
            margin-top: 12px;

        }

        .radio,
        .khoa {
            margin-left: 20px;
            border: 1px solid rgb(87, 137, 245);
        }

        .lab {
            background: cornflowerblue;
            padding: 8px 10px;
            border: 2px solid blue;
            width: 25%;
            display: block;
            color: white;
        }

        .inp {
            margin-left: 20px;
            width: 60%;
            border: 1px solid rgb(87, 137, 245);
        }

        button {
            background: rgb(4, 129, 44);
            padding: 8px 10px;
            border: solid 2px rgb(154, 154, 232);
            width: 25%;
            display: block;
            color: white;
            border-radius: 6px;
        }

        .submit {
            display: flex;
            justify-content: center;
        }

        .gioitinh-wrapper > div {
            margin-left: 20px;
            margin-top: 8px;
        }

        #date {
            margin-left: 20px;
        }

        .required {
            color: red;
            font-weight: bold;
            margin-left: 3px;
        }

        .errors > div {
            color: red;
            font-weight: 600;
        }

        .label_right {
            margin-left: 20px;
            margin-top: 10px;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="wrapper">

        <div>


            <form action="#" method="get">

                <div class="name">
                    <label class="lab">Họ và tên <span class="required">*</span></label>
                    <div class="label_right"><?=$data['name']?></div>
                </div>
                <div class="name gioitinh-wrapper">
                    <label class="lab">Giới tính <span class="required">*</span></label>
                    <div class="label_right"><?=$data['gender']?></div>
                </div>
                <div class="name">
                    <label class="lab">Phân Khoa <span class="required">*</span></label>
                    <div class="label_right"><?=$data['department']?></div>
                </div>
                <div class="name">
                    <label class="lab">Ngày sinh <span class="required">*</span></label>
                    <div class="label_right"><?=$data['birthday']?></div>
                </div>
                <div class="name">
                    <label class="lab">Địa chỉ</label>
                    <div class="label_right"><?=$data['address']?></div>
                </div>
                <div class="name">
                    <label class="lab">Ảnh</label>
                    <div class="label_right">
                        <img width="100" height="100" src="<?=$data['img']?>" alt="">
                    </div>
                </div>
                <div class="submit">
                    <button>Đăng kí</button>
                </div>
            </form>
        </div>

    </div>

</div>
</div>
</body>
<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
<script type="text/javascript"
        src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>


</html>
